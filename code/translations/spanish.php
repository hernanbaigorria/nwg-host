<?php 

// Configuracion del sitio y paginas
	$lang['configurations_site_title']						= 'Título del Sitio';
	$lang['configurations_page_title']						= 'Título de Página';
	$lang['configurations_main_page']						= 'Página Principal';
	$lang['configurations_search_results_page']				= 'Página de resultados de búsqueda (no disponible en esta web)';
	$lang['configurations_page_description']				= 'Descripción de página';
	$lang['configurations_page_tags']						= 'Tags de página';
	$lang['configurations_page_ganalitics']					= 'Código de google analytics';
	$lang['configurations_page_top_menu_opts']				= 'Opciones del menu superior';
	
	$lang['configurations_general_phone']					= 'Número de teléfono a mostrar';
	$lang['configurations_general_address']					= 'Dirección a mostrar';
	$lang['configurations_general_whatsapp']				= 'Número de whatsapp a mostrar';
	$lang['configurations_general_mail']					= 'Dirección de email a mostrar';
	$lang['configurations_general_mail2']					= 'Dirección de email secundaria';
	$lang['configurations_page_contact']					= 'Página de contacto';
	$lang['configurations_general_minmenu']					= 'Menu barra superior';
	$lang['configurations_page_favicon']					= 'Favicon';


	$lang['configurations_img_slider']						= 'Imagenes slider';
	$lang['configurations_cantidad_a_mostrar']				= 'Cantidad de imagenes';

	$lang['configurations_body_html']						= 'Cuerpo html';


	// Ver que estos tienen antepuesto un BR, es como para agruparlos un poco..
	$lang['configurations_page_main_menu_name1']			= '<h3>Menú Principal</h3><strong>Opción 1</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link1']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts1']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name2']			= '<br><strong>Opción 2</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link2']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts2']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name3']			= '<br><strong>Opción 3</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link3']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts3']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name4']			= '<br><strong>Opción 4</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link4']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts4']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name5']			= '<br><strong>Opción 5</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link5']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts5']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name6']			= '<br><strong>Opción 6</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link6']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts6']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name7']			= '<br><strong>Opción 7</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link7']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts7']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name8']			= '<br><strong>Opción 8</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link8']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts8']			= 'Opciones Submenu';

	// Footer tambien tiene tags para que sea mas entendible.
	$lang['configurations_page_foot_intro']					= '<h3>Pie de Página</h3>Texto de intro';
	$lang['configurations_page_foot_menu_t1']				= '<br>Título Columa 1';
	$lang['configurations_page_foot_menu_t2']				= '<br>Título Columa 2';
	$lang['configurations_page_foot_menu_p1']				= '<h3>Menú Footer</h3>';
	$lang['configurations_page_foot_menu_p2']				= 'Links Columa 2';

	// Otras paginas.
	$lang['configurations_page_social_facebook']			= 'Página de Facebook (Externa)';
	$lang['configurations_page_social_twitter']				= 'Página de Twitter (Externa)';
	$lang['configurations_page_social_vimeo']				= 'Página de Vimeo (Externa)';
	$lang['configurations_page_social_instagram']			= 'Página de Instagram (Externa)';
	$lang['configurations_page_social_youtube']				= 'Página de Youtube (Externa)';
	$lang['configurations_page_social_spotify']				= 'Página de Spotify (Externa)';
	$lang['configurations_page_social_linkedin']			= 'Página de Linkedin (Externa)';
	$lang['configurations_page_visible_title']				= 'Título visible de página';
	$lang['configurations_404_page']						= 'Pagina de error 404';