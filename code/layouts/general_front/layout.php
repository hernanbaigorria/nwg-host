    <!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="<?php echo @$PAGE_CFG['page_description'] ?>">
        <meta name="keywords" content="<?php echo @$PAGE_CFG['page_tags'] ?>">

        <link rel="icon" href="<?php echo media_uri($PAGE_CFG['page_favicon']) ?>">
        <title><?php echo @$PAGE_CFG['site_title'] ?><?php echo (!empty($PAGE_CFG['page_title'])) ? ' - '.$PAGE_CFG['page_title'] : NULL ?></title>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_HEADER ?>
        <?php endif ?>
        <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom.css" rel="stylesheet">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/all.min.css" rel="stylesheet">
        
        <?php if ($EDITABLE): ?>
            <link href="<?php echo THEME_ASSETS_URL ?>general/css_new/bootstrap.min.css" rel="stylesheet">
            <script src="<?php echo static_url('global/plugins/jquery.min.js') ?>"></script>
        <?php else: ?>
           
        <?php endif ?>

        
    </head>
    <body class="">
            <!-- Header -->
            <header>
                <div class="top-header">
                    <div class="container">
                        <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                            <a href="tel:<?php echo $PAGE_CFG['general_phone'] ?>" class="d-inline-flex align-items-center justify-content-center flex-wrap">
                                <i class="fas fa-phone-alt"></i>
                                <p><?php echo $PAGE_CFG['general_phone'] ?></p>
                            </a>
                        <?php endif ?>
                        <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                            <a href="mailto:<?php echo $PAGE_CFG['general_mail'] ?>" class="d-inline-flex align-items-center justify-content-center flex-wrap">
                                <i class="fas fa-envelope-open-text"></i>
                                <p><?php echo $PAGE_CFG['general_mail'] ?></p>
                            </a>
                        <?php endif ?>
                        <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank" class="float-right">
                                <i class="fab fa-facebook-square" style="font-size: 21px;"></i>
                            </a>
                        <?php endif ?>
                    </div>
                </div>
                <div class="container">
                    <nav class="navbar navbar-toggleable-md navbar-light">
                      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      <a class="navbar-brand" href="<?php echo base_url() ?>">
                          <img src="<?php echo media_uri($PAGE_CFG['logo_header']) ?>" class="img-fluid">
                      </a>

                      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                        <?php 
                            for ($i=1; $i <= 6; $i++) { 
                            ?>
                            <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                            <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                                <li class="nav-item <?php if ($ID_PAGE == $PAGE_CFG['page_main_menu_link'.$i] OR in_array($ID_PAGE, $gtp_pages)) echo 'active' ?>">
                                    <?php 
                                        $this_url = page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE); 
                                        $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                    ?>
                                    <a target="<?php echo $target ?>" class="nav-link" href="<?php echo $this_url ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                                </li> 
                            <?php endif ?>
                            <?php }
                            ?>
                        </ul>
                      </div>
                    </nav>
                </div>
            </header>
            <!-- Start main-content -->
            <div class="page_components_containers">
                <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
            </div>

            <?php echo @$MODULE_BODY; ?>
            <!-- end main-content -->

            <!-- Footer -->
            <footer>
                <div class="newsletter-franja">
                    <div class="container">
                        <div class="d-flex align-items-center justify-content-center flex-wrap" style="border-bottom:1px #fff solid;padding-bottom:15px;">
                            <div class="icnos">
                                <a href="#" style="font-size:30px;">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                                <a href="#">
                                    <i class="fas fa-envelope-open-text"></i>
                                </a>
                            </div>
                            <p>Dejanos tu e-mail y te contactamos</p>
                            <form action="#" method="POST">
                                <input type="email" name="email" placeholder="e-mail">
                                <input type="submit" value="suscribirme">
                            </form>
                        </div>
                    </div> 
                </div>
                <div class="container footer-info">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <img src="<?php echo media_uri($PAGE_CFG['logo_header']) ?>" class="img-fluid logo-foot">
                        </div>
                        <div class="col-12 col-md-6 text-r d-flex justify-content-end">
                            <div class="info-contact">
                                <h3>Departamentos</h3>
                                <hr>
                                <ul>
                                    <li><p>- Córdoba</p></li>
                                    <li><p>- Carlos Paz</p></li>
                                    <li><p>- Corporativos</p></li>
                                </ul>
                            </div>
                            <div class="info-contact">
                                <h3>Contacto</h3>
                                <hr>
                                <ul>
                                    <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                                        <li>
                                            <a href="tel:<?php echo $PAGE_CFG['general_phone'] ?>" class="d-inline-flex align-items-center justify-content-center flex-wrap">
                                                <i class="fas fa-phone-alt"></i>
                                                <p><?php echo $PAGE_CFG['general_phone'] ?></p>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                                        <li>
                                            <a href="mailto:<?php echo $PAGE_CFG['general_mail'] ?>" class="d-inline-flex align-items-center justify-content-center flex-wrap">
                                                <i class="fas fa-envelope-open-text"></i>
                                                <p><?php echo $PAGE_CFG['general_mail'] ?></p>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                                        <li>
                                            <a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank" class="d-inline-flex align-items-center justify-content-center flex-wrap">
                                                <i class="fab fa-facebook-square" style="font-size: 21px;"></i>
                                                <p>hostalquileres</p>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="franja-foot">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-6 text-left">
                                <p>Copyright <?php echo date("Y"); ?></p>
                            </div>
                            <div class="col-12 col-md-6 text-right">
                                <p>Powered by <a href="https://www.inglobe.com.ar/" target="_blank">INGLOBE S.R.L</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- Modal -->
            <div class="modal fade" id="modal-asistir" tabindex="-1" role="dialog" aria-labelledby="mod-asistir" aria-hidden="true" style="z-index: 99999;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mod-asistir"><?php echo $PAGE_CFG['mod_asis_title'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php echo $PAGE_CFG['mod_asis_body'] ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $PAGE_CFG['mod_asis_btn'] ?></button>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($EDITABLE): ?>
                <?php echo $EDITOR_FOOTER ?>
            <?php else: ?>
                <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
                <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>
            <?php endif ?>
            
            <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js" data-wtf="Dependencia de BS4"></script>
            <script src="<?php echo THEME_ASSETS_URL ?>general/js/bootstrap.min.js"></script>
            <script src="<?php echo THEME_ASSETS_URL ?>general/js/custom.js"></script>
    
    </body>
</html>