<div class="col-12 franja-destacados">
    <h3>CONSULTANOS POR <strong>ALQUILERES CORPORATIVOS</strong></h3>
</div>

<div class="container destacados">
    <div class="row row-padding">
        <div class="col-12 col-md-2 p-0">
            <h2>DESTACADOS</h2>
            <hr>
        </div>
        <div class="col-12 col-md-10"></div>
        <div class="col-12 col-md-3">
            <a href="#" class="box d-inline-flex flex-column" style="background:url('<?php echo THEME_ASSETS_URL ?>general/images/destacada_img.jpg');">
                <div class="franja-box">
                    <p>RESERVAR</p>
                </div>
                <div class="franja-info-01 d-flex align-items-center justify-content-center">
                    <div class="icon">
                        <img src="<?php echo THEME_ASSETS_URL ?>general/images/icon_map.png" class="img-fluid">
                    </div>
                    <h4>General Paz 848 - Bº Centro</h4>
                </div>
                <div class="franja-info-02 d-flex align-items-center justify-content-center">
                    <div class="icon">
                        <img src="<?php echo THEME_ASSETS_URL ?>general/images/icon_money.png" class="img-fluid">
                    </div>
                    <h5>DIA: $1500 | MES: $1500</h5>
                </div>
                <div class="absolute-info d-flex align-items-center justify-content-center flex-wrap">
                    <div class="info-text">
                        <h3>General Paz 333</h3>
                        <div class="franja-info-02 d-flex align-items-center justify-content-center">
                            <div class="icon">
                                <img src="<?php echo THEME_ASSETS_URL ?>general/images/icon_money_v2.png" class="img-fluid">
                            </div>
                            <h5>$444 por día</h5>
                        </div>
                        <p>Ver detalles</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>