<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['backgroud_img']['default']					= NULL;
	$ADDED_CFG['backgroud_img']['multilang'] 				= FALSE;
	$ADDED_CFG['backgroud_img']['permissions'] 				= FALSE;
	$ADDED_CFG['backgroud_img']['type'] 					= 'image';
	$ADDED_CFG['backgroud_img']['max'] 						= 1;


	$ADDED_CFG['backgroud_img_02']['default']					= NULL;
	$ADDED_CFG['backgroud_img_02']['multilang'] 				= FALSE;
	$ADDED_CFG['backgroud_img_02']['permissions'] 				= FALSE;
	$ADDED_CFG['backgroud_img_02']['type'] 					= 'image';
	$ADDED_CFG['backgroud_img_02']['max'] 						= 1;